/*******************************************************************************
	Підключаєм потрібні бібліотеки та плагіни
*******************************************************************************/
const path = require('path')
const fs = require('fs-extra')
const webpack = require('webpack')

const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const YamastersBackendtree = require('yamasters-backendtree-webpack-plugin-new');


/*******************************************************************************
	Константи проекту
*******************************************************************************/
const PROJECT_NAME = 'Vue simple';


/*******************************************************************************
	Формуєм список файлів, які будуть згенеровані

	В корні папки проекту містяться штмл-файли, які є шаблонами майбутніх сторінок
	сайту. Вони складаються із базової сітки та містять блоки, що імпортуються.
	Ці штмл-сторінки перетворяться в пхп-сторінки після побудови проекту в режимі
	"продакшн".
*******************************************************************************/

// список всіх файлів в корні папки проекту
let files = fs.readdirSync(__dirname);

/**
	Список штмл-файлів, які треба проігнорувати при компіляції проекту
	@type  {boolean|Array<String>}
*/
// let ignorelist = false;

let ignorelist = require('./.ignorelist').arr;
/**
	Список js-файлів, які будуть включені в компіляцію проекта.
	Ці файли є частиною конфігурації вебпака, секція entries
	@type  {Object}
*/
let entries = {};

/**
	Список папок, які треба скопіювати в побудований проект (шрифти, зображення і тд)
	@type  {Array<String>}
*/
let folders_for_copying = ['fonts', 'api'];
let fpfc = [
	{
		from: path.resolve(__dirname, `.tmp`),
		to: path.resolve(__dirname, `php/bundles`)
	}
];

folders_for_copying.map(folder => {
	fpfc.push({
		from: path.resolve(__dirname, `src/${folder}`),
		to: path.resolve(__dirname, `dist/${folder}`)
	})
})

/**
	Масив плагінів HtmlWebpackPlugin для кожної із сторінок, що буде включена
	в процес збірки проекту.
	@type  {Array<HtmlWebpackPlugin>}
*/
let pages = [];


/*
Якщо список файлів для ігнору не пустий, то треба виключити такі сторінки із
процесу збірки проекту. Це зекономить час на збірку проекту.
*/
files.forEach(file => {
	if(path.extname(file) === '.html') {
		let name = path.basename(file, path.extname(file));
		let flag = true;

		if(process.env.NODE_ENV === 'production') {
			ignorelist = null;
		}

		if(ignorelist) {
			for(let i = 0; i < ignorelist.length; i++) {
				if(name == ignorelist[i]) {
					flag = false;
					break;
				}
			}
		}

		if(flag) {
			entries[name] = `./entries/${name}.js`;

			pages.push(new HtmlWebpackPlugin({
				template: path.resolve(__dirname, `${name}.html`),
				filename: path.resolve(__dirname, `dist/${name}.html`),
				chunks: ['commons', name],
				title: `${PROJECT_NAME} | ${name}`
			}))
		}
	}
})

// console.log(entries);

const HTML_extracter = new ExtractTextPlugin({
	filename: '[name].html',
	allChunks: true
});

const CSS_extracter = new ExtractTextPlugin({
	filename: 'bundles/[name].css',
	allChunks: true
})

module.exports = {
	entry: entries,

	output: {
		path: path.resolve(__dirname, 'dist'),
		publicPath: '', // шлях до бандлів
		filename: 'bundles/[name].bundle.js'
	},

	module: {
		rules: [
			{
				test: /\.vue$/,
				loader: 'vue-loader',
				options: {
					loaders: {
						scss: CSS_extracter.extract({
							use: [
								{
									loader: 'css-loader',
									options: {
										importLoaders: 2,
										minimize: process.env.NODE_ENV === 'production' ? true : false,
										url: false
									}
								},
								{
									loader: 'postcss-loader'
								},
								{
									loader: 'sass-loader',
									options: {
										includePaths: ['scss']
									}
								}
							]
						})
					}
				}
			},
			{
				test: /\.js$/,
				loader: 'babel-loader',
				include: [
					path.resolve(__dirname, './entries'),
					path.resolve(__dirname, './libs'),
					path.resolve(__dirname, './src/includes'),
					path.resolve(__dirname, './node_modules/@yamasters')
				]
			},
			{
				test: /\.html$/,
				use: [
					{
						loader: 'html-loader',
						options: {
							minimize: false,
							interpolate: 'require',
							attrs: ['require:url']
						}
					},
					{
						loader: 'html-component-loader',
						options: {
							src: __dirname
						}
					}
				]
			},
			{
				test: /\.s?css$/,
				use: CSS_extracter.extract({
					use: [
						{
							loader: 'css-loader',
							options: {
								importLoaders: 1,
								minimize: process.env.NODE_ENV === 'production' ? true : false,
								url: false
							}
						},
						{
							loader: 'postcss-loader'
						},
						{
							loader: 'sass-loader',
							options: {
								outputStyle: 'expanded',
								includePaths: ['scss']
							}
						}
					]
				})
			},
			{
				test: /\.(png|jpg|gif|svg)$/,
				loader: 'ignore-loader'
			}
		]
	},

	resolve: {
		alias: {
			'__components': path.resolve(__dirname, 'src/includes/components'),
			'__assets': path.resolve(__dirname, 'src/assets'),
			'__scss': path.resolve(__dirname, 'scss'),
			'__page': path.resolve(__dirname, ''),
			'__libs': path.resolve(__dirname, 'libs')
		}
	},

	devServer: {
		contentBase: ['dist', 'src'],
		port: 505,
		host: require('../WORKSPACE_SETTINGS.js').IP,
		noInfo: true,
		// hot: true
	},

	performance: {
		hints: false
	},

	devtool: 'cheap-module-source-map',

	plugins: [
		new CleanWebpackPlugin(['dist/*', 'php/*', '.tmp/*'], {
			verbose: true
		}),
		new webpack.optimize.CommonsChunkPlugin({
			name: 'commons',
			filename: 'bundles/commons.js'
		}),
		CSS_extracter,
		new webpack.ProvidePlugin({
			$: "jquery",
			jQuery: "jquery",
			"window.jQuery": "jquery",
			Vue: ['vue/dist/vue.esm.js', 'default'],
			VueResource: ['vue-resource', 'default']
		}),
		// new webpack.NamedModulesPlugin(),
		// new webpack.HotModuleReplacementPlugin()
	].concat(pages)
}

if(process.env.NODE_ENV === 'production') {
	var fileContent;
	try {
		let fVersion = fs.readFileSync("version.json", "utf8");

		var numb = Number(fVersion);
		numb++;
		fs.writeFileSync("version.json", numb);

		console.log('new version = ' + numb);

		fileContent = numb;

	} catch (e) {
		fileContent = 0;
	}

	// *****************************************************************************
	// сжатие картинок
	// *****************************************************************************
	const imagemin = require('imagemin');
	const imageminJpegtran = require('imagemin-jpegtran');
	// const imageminPngquant = require('imagemin-pngquant');
	const imageminSvgo = require('imagemin-svgo');
	const imageminMozjpeg = require('imagemin-mozjpeg');

	(async () => {
		const files = await imagemin(['src/img/*'], 'php/img', {
			plugins: [
				imageminJpegtran({progressive: true}),
				// imageminPngquant({quality: '65-80'}),
				imageminMozjpeg({
	        quality: 85
	    	}),
				imageminSvgo({
					plugins: [
					{removeViewBox: true}
				]
	})
			]
		});

		console.log(files);
	})();

	module.exports.devtool = '#source-map';

	module.exports.plugins = (module.exports.plugins || []).concat([
		new CopyWebpackPlugin(fpfc ,
		{copyUnmodified: true}),

		new YamastersBackendtree({
			from: path.resolve(__dirname, 'dist'),
			to: path.resolve(__dirname, 'php')
		}),

		new webpack.DefinePlugin({
			'process.env': {
				NODE_ENV: '"production"'
			},
			'PROJECT_VERSION': fileContent
		}),


		new webpack.optimize.UglifyJsPlugin({
			sourceMap: true,
			compress: {
				warnings: false
			}
		}),

		new webpack.optimize.MinChunkSizePlugin({
			minChunkSize: 10000
		}),

		new webpack.LoaderOptionsPlugin({
			minimize: true
		})
	]);
}
