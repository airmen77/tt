/*
	Компілюється у файл
*/
process.env.NODE_ENV === 'production' ? console.log('Production mode!') : require('__page/index.html');

/*
	Підключаєм конструктор модулів
*/
const mc = require('__libs/ModuleConstructor.js');
require('rangeslider.js');

const validators = require('vuelidate');
// const { required, minLength } = window.validators

Vue.use(validators.default)


/**
	Запускаєм конструктор модулів. Аргументом приймає масив назв vue-компонентів,
	які треба включити в процес збірки проекту.
	@type {Array<String>}
*/
mc(['Calc', 'Slider']);

/*
	Запускаєм скріпти html компонентів
*/
STACK.forEach(f => {
	f($);
})
