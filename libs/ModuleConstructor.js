window.testEnv = process.env.NODE_ENV;

if(window.testEnv === 'production') {
	window.ProjectVersion = PROJECT_VERSION;
	if(window.ProjectVersion){
		console.log('project version = ' + window.ProjectVersion);
	}
}
/*
	Підключаєм тільки ті стилі, які будуть на всіх сторінках
*/
require('__scss/grid.scss');					// сітка
require('__scss/text.scss');					// стилі тектсу
require('__scss/fonts.scss');					// шрифти
require('__scss/awesome.scss');				// іконци
require('__scss/jquery.fancybox.scss');	// стилі фансібокса


/*
	Підключаєм тільки ті модулі, які беруть участь в логіці всіх сторінок
*/
const fancybox = require('fancybox')($);

window.Inputmask = require('inputmask');

const { validationMixin, default: Vuelidate } = require('vuelidate');
const { required, minLength } = require('vuelidate/lib/validators');


window.$ = $;
window.fancybox = fancybox;

require('__libs/validator.js');

require('__libs/share.js');

$('body').on('ajaxResponse', function() {
	function EXE(f) {
		var a = setInterval(function() {
			if(window.allScriptsAreLoaded) {
				clearInterval(a);
				console.log("All async scripts are loaded!");
				f();
			}
			else {
				console.log("Waiting for scripts...");
			}
		}, 300);
	}

	module.exports = (array) => {
		// console.log(array);

		if(array && array.length) {
			window.VueInstances = {};

			Vue.use(VueResource);
		

			array.map(component => {
				Vue.component(component, require(`__components/${component}.vue`));
				window.VueInstances[component] = {};
			})

			const components = document.querySelectorAll('[is]');

			// шукаєм всі діви із атрибутом "is" та запускаєм компоненти
			for(let i = 0; i < components.length; i++) {

				// перевіряєм, чи включений компонент в процес збірки проекту
				if(window.VueInstances[components[i].getAttribute('is')]) {
					if(components[i].getAttribute('data-params')) {

						let str = components[i].getAttribute('data-params');

						str = str.replace(/'/g, '"');

						let params = JSON.parse(str);

						window.VueInstances[components[i].getAttribute('is')][params.template] = new Vue({ el: components[i] });
					}
					else {
						console.warn('dataParams is no defined', components[i]);
					}
				}
				else {
					console.warn(`Component ${components[i].getAttribute('is')} is not enabled, but used! Check module constructor options.`);
				}
			}
		}
	}
});




$("body").trigger('ajaxResponse');


NodeList.prototype.forEach = function(f) {
	for(var i = 0; i < this.length; i++) {
		f(this[i], i);
	}
};

(function (ElementProto) {
	if (typeof ElementProto.matches !== 'function') {
		ElementProto.matches = ElementProto.msMatchesSelector || ElementProto.mozMatchesSelector || ElementProto.webkitMatchesSelector || function matches(selector) {
			var element = this;
			var elements = (element.document || element.ownerDocument).querySelectorAll(selector);
			var index = 0;

			while (elements[index] && elements[index] !== element) {
				++index;
			}

			return Boolean(elements[index]);
		};
	}

	if (typeof ElementProto.closest !== 'function') {
		ElementProto.closest = function closest(selector) {
			var element = this;

			while (element && element.nodeType === 1) {
				if (element.matches(selector)) {
					return element;
				}

				element = element.parentNode;
			}

			return null;
		};
	}
})(window.Element.prototype);