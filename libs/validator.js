const YamParam = window.YAMASTERS_FRONTEND;

$('form').on('keydown',function(e){
	if(e.keyCode == 13) {
		e.preventDefault();
		return false;
	}
});

$('body').on('ajaxResponse', function() {

	let phone = new Inputmask({
		mask: window.YAMASTERS_FRONTEND.validator.mask.tel.default,
		definitions: {
			'#': '',
			'^': {
				validator: "[0-9\\s]",
				cardinality: 1,
				casing: "lower"
			}
		},
		oncomplete: (e) => {
			// console.log(e.target);
			console.log('tel oncomplete');
			e.target.classList.remove('onincomplete');
			e.target.classList.add('oncomplete');
		},
		onincomplete: (e) => {
			console.log('tel onincomplete');
			e.target.classList.remove('oncomplete');
			e.target.classList.add('onincomplete');
		},
		oncleared: () => {
			console.log('tel oncleared');
			completePhone = false;
			clearIncomplete  = true;
		}
	});

	let email = new Inputmask({
		mask: window.YAMASTERS_FRONTEND.validator.mask.email,
		greedy: false,
		definitions: {
			'*': {
				validator: "[0-9A-Za-z_-]",
				cardinality: 1,
				casing: "lower"
			},
			'@': {
				validator: '@',
				cardinality: 1,
				casing: 'lower'
			},
			'd': {
				validator: '.',
				cardinality: 1,
				casing: 'lower'
			}
		},
		oncomplete: (e) => {
			// console.log('oncomplete');
			e.target.classList.remove('onincomplete');
			e.target.classList.add('oncomplete');
		},
		onincomplete: (e) => {
			e.target.classList.remove('oncomplete');
			e.target.classList.add('onincomplete');
		},
		oncleared: () => {
			completeEmail = false;
		}
	});


	phone.mask($('[type="tel"]'));
	email.mask($('.email'));
	$('.email').val('');


	if(YamParam.forms == undefined) {
		YamParam.forms = {};
	}
	if(YamParam.forms.beforEventsArray == undefined) {
		YamParam.forms.beforEventsArray = [];
	}
	if(YamParam.forms.afterEventsArray == undefined) {
		YamParam.forms.afterEventsArray = [];
	}
	/**
	* Проверка валидности веб форм
	* входные параметры - e - event, оbj - $(this) функции
	* return вернет значение логическое и повесит клас на веб форму
	*/
	YamParam.forms.yamCheckValidForm = function(e, obj) {
		e.preventDefault();
		let _this = obj;
		let inp = _this.closest('form');
		// inp.addClass('noSend');
		inp.find('input').on('focus', function () {
			$(this).removeClass('errorFild');
		});
		inp.find('textarea').on('focus', function () {
			$(this).removeClass('errorFild');
		});

		if(obj.closest('form').find('[type="tel"]')){
			obj.closest('form').find('[type="tel"]').trigger('focus');
			obj.closest('form').find('[type="tel"]').trigger('blur');
		}

		if(obj.closest('form').find('.email')){
			obj.closest('form').find('.email').trigger('focus');
			obj.closest('form').find('.email').trigger('blur');
		}


		obj.closest('form').find('input').each(function () {
			if($(this)[0].type == "tel"){
				if($(this).hasClass('oncomplete')){
					$(this).removeClass('noSend');
				}
				else{
					if($(this).hasClass('onincomplete')){
						$(this).val("");
						$(this).val($(this).attr('value'));
					}
					$(this).addClass('noSend');
				}
			}
			else if($(this).hasClass('email')){
				if($(this).hasClass('oncomplete')){
					$(this).removeClass('noSend');
				}
				else{
					$(this).addClass('noSend');
				}
			}
			else if($(this).attr('required')){
				if($(this)[0].type == "checkbox"){
					if($(this)[0].checked){
						$(this).closest('label').removeClass('errorFild');
						$(this).removeClass('noSend');
					}else {
						$(this).closest('label').addClass('errorFild');
						$(this).addClass('noSend');
					}
				}
				else if($(this).val() == ""){
					$(this).addClass('noSend');
				}else {
					$(this).removeClass('noSend');
				}
			}
			if($(this)[0].value == ""){
				$(this).addClass('errorFild');
			}
		});

		inp.removeClass('noSend');
		obj.closest('form').find('input').each(function () {
			if($(this).hasClass('noSend')){
				inp.addClass('noSend');
			}
		});

		if(obj.closest('form').find('textarea').length){
			if(obj.closest('form').find('textarea').attr('required')){
				if(obj.closest('form').find('textarea').val() == ""){
					obj.closest('form').find('textarea').addClass('errorFild');
					inp.addClass('noSend');
				}
			}
		}

		if(!inp.hasClass('noSend')){
			console.log('true');
			return true;
		} else {
			console.log('false');
			return false;
		}
	}

	YamParam.forms.mainEvent = function(e, obj) {
		YamParam.forms.beforEvents (e, obj);
		var successValidation = false;
		if (YamParam.forms.yamCheckValidForm(e, obj)) {
			successValidation = true;
		}
		YamParam.forms.afterEvents (e, obj, successValidation);

		if (successValidation) {
			obj.closest('form').find('[type="submit"]').click();
		}
	}

	YamParam.forms.beforEvents = function (e, obj) {
		if (YamParam.forms.beforEventsArray) {
			// console.log('beforEventsArray');
			for (var i = 0; i < YamParam.forms.beforEventsArray.length; i++) {
				YamParam.forms.beforEventsArray[i] (e, obj);
			}
		}
	}

	YamParam.forms.afterEvents = function (e, obj, valid) {
		// console.log(valid);
		if (YamParam.forms.afterEventsArray) {
			// console.log('afterEventsArray');
			for (var i = 0; i < YamParam.forms.afterEventsArray.length; i++) {
				YamParam.forms.afterEventsArray[i] (e, obj, valid);
			}
		}
	}

	// ================================================================

	$(document).on('click', '[data-submitter]', function (e) {
		YamParam.forms.mainEvent(e, $(this));
	});

	// ============================================================

	// ============================================================
	//  js class prototype not used
	// ============================================================

	//  function ValidForm(){};
	//
	//  ValidForm.prototype.main = function(e, obj){
	// 		e.preventDefault();
	// 		console.log(obj);
	// 		let _this = obj;
	// 		let inp = _this.closest('form');
	// 		// inp.addClass('noSend');
	// 		inp.find('input').on('focus', function () {
	// 			$(this).removeClass('errorFild');
	// 		});
	//
	// 		if(obj.closest('form').find('[type="tel"]')){
	// 			obj.closest('form').find('[type="tel"]').trigger('focus');
	// 			obj.closest('form').find('[type="tel"]').trigger('blur');
	// 		}
	//
	// 		if(obj.closest('form').find('.email')){
	// 			obj.closest('form').find('.email').trigger('focus');
	// 			obj.closest('form').find('.email').trigger('blur');
	// 		}
	//
	//
	// 		obj.closest('form').find('input').each(function () {
	// 			if($(this)[0].type == "tel"){
	// 				if($(this).hasClass('oncomplete')){
	// 					inp.removeClass('noSend');
	// 				}
	// 				else{
	// 					if($(this).hasClass('onincomplete')){
	// 						$(this).val("");
	// 						$(this).val($(this).attr('value'));
	// 					}
	// 					inp.addClass('noSend');
	// 					return false
	// 				}
	// 			}
	// 			else if($(this).hasClass('email')){
	// 				if($(this).hasClass('oncomplete')){
	// 					inp.removeClass('noSend');
	// 				}
	// 				else{
	// 					inp.addClass('noSend');
	// 					return false
	// 				}
	// 			}
	// 			else if($(this)[0].value == ""){
	// 				$(this).addClass('errorFild');
	// 			}
	// 		});
	// 		if(!inp.hasClass('noSend')){
	// 			return true;
	// 		} else {
	// 			return false;
	// 		}
	// 	}
	//
	//  ValidForm.prototype.before  = function(e, obj){
	// 	console.log('before  ' + obj);
	// 	return obj
	// }
	//
	//  ValidForm.prototype.after  = function(e, obj, valid){
	// 	console.log('after ' + obj);
	// 	return obj, valid
	// }


	// console.log('ajaxResponse run!!!!');
});
