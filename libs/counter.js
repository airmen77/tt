//////////////////////////////////////////////////////
// yamasters counter

﻿var yamCounter = function() {
	var T, I, A;

	var getCounterSettings = function(el) {
		var min, max, step, float;

		if($(el).closest("[data-yam-counter]")[0].getAttribute('data-yam-counter') != '') {
			var arr = $(el).closest("[data-yam-counter]")[0].getAttribute('data-yam-counter');
			arr = arr.replace("[", "");
			arr = arr.replace("]", "");
			arr = arr.split(",");

			float = parseInt(arr[3]);

			if(float) {
				min = parseFloat(arr[0]);
				max = parseFloat(arr[1]);
				step = parseFloat(arr[2]);
			}
			else {
				min = parseInt(arr[0]);
				max = parseInt(arr[1]);
				step = parseInt(arr[2]);
			}
		}
		else {
			min = 1;
			max = 9999;
			step = 1;
		}

		return {
			min: min,
			max: max,
			step: step,
			float: float
		}
	}

	var applyValue = function(el, val, settings) {
		if(settings.float) {
			val = Math.round(val * Math.pow(10, settings.float)) / Math.pow(10, settings.float);
			val = val.toFixed(settings.float);
		}
		else {
			val = parseInt(val);
		}

		el.value = val;
	}

	var getValue = function(el, settings) {
		var val;

		if(settings.float) {
			val = parseFloat(el.value);
		}
		else {
			val = parseInt(el.value);
		}

		return val;
	}

	var downEvent, upEvent;

	if(window.isTouchDevice()) {
		// downEvent = "touchstart";
		// upEvent = "touchend";
		downEvent = "mousedown";
		upEvent = "mouseup";
	}
	else {
		downEvent = "mousedown";
		upEvent = "mouseup";
	}

	document.addEventListener("keyup", function(e) {
		if(e.target.getAttribute("data-counter") == "field") {

			var settings = getCounterSettings(e.target);

			if(e.keyCode == 13) {
				var s = parseFloat(e.target.value);

				if(isNaN(s) || s == null || s == "") s = min;
				if(s > settings.max) s = settings.max;
				// e.target.value = s;
				applyValue(e.target, s, settings);

				var id = e.target.parentElement.getAttribute("data-trigger-refresh");

				if(id) {
					if(document.getElementById(id)) {
						$(e.target).bind("change", function() {
							setTimeout(function() {
								document.getElementById(id).click();
							}, 1500);
						});
					}
					else {
						console.info("Кнопка #"+ id +", по якій емулюється клік, не знайдена");
					}
				}

				$(e.target).trigger("change");
				$(e.target).unbind("change");
				e.target.blur();

				$('body').trigger('yamasters-counter:changed', {source: e.target});
			}
		}
	});

	document.addEventListener(downEvent, function(e) {
		// console.log(downEvent);
		if(e.target.getAttribute("data-counter") == "plus" || e.target.getAttribute("data-counter") == "minus") {
			e.preventDefault();
			e.target.classList.add("pressed");
			var field = e.target.parentElement.querySelector('[data-counter="field"]');
			field.classList.add("focus");
			var num = 0;

			var settings = getCounterSettings(e.target);

			if(e.target.getAttribute('data-counter') == 'plus') {
				T = setTimeout(function() {
					I = setInterval(function() {
						// num = parseInt(field.value);
						num = getValue(field, settings);
						if(num == '' || num == null || isNaN(num)) num = 0;
						num += settings.step;
						if(num > settings.max) num = settings.max;
						// field.value = num;
						applyValue(field, num, settings);
						$(field).triggerHandler("focus");
						$(field).triggerHandler("blur");
					}, 100);
				}, 500);
			}
			if(e.target.getAttribute('data-counter') == 'minus') {
				T = setTimeout(function() {
					I = setInterval(function() {
						// num = parseInt(field.value);
						num = getValue(field, settings);
						if(num == '' || num == null || isNaN(num)) num = 0;
						num -= settings.step;
						if(num < settings.min) num = settings.min;
						// field.value = num;
						applyValue(field, num, settings);
						$(field).triggerHandler("focus");
						$(field).triggerHandler("blur");
					}, 100);
				}, 500);
			}

			$('body').trigger('yamasters-counter:changing', {source: field});
		}
	});

	document.addEventListener(upEvent, function(e) {
		if(e.target.getAttribute("data-counter") == "plus" || e.target.getAttribute("data-counter") == "minus") {
			e.preventDefault();
			e.target.classList.remove("pressed");
			clearTimeout(T);
			clearTimeout(A);
			clearInterval(I);
			var field = e.target.parentElement.querySelector('[data-counter="field"]');
			field.classList.remove("focus");
			var num = 0;

			var settings = getCounterSettings(e.target);
			// console.log(settings);

			if(e.target.getAttribute('data-counter') == 'plus') {
				// num = parseInt(field.value);
				num = getValue(field, settings);
				if(num == '' || num == null || isNaN(num)) num = 0;
				num += settings.step;
				if(num > settings.max) num = settings.max;
				// field.value = num;
				applyValue(field, num, settings);
				$(field).triggerHandler("focus");
				$(field).triggerHandler("blur");
			}
			if(e.target.getAttribute('data-counter') == 'minus') {
				// num = parseInt(field.value);
				num = getValue(field, settings);
				if(num == '' || num == null || isNaN(num)) num = 0;
				num -= settings.step;
				if(num < settings.min) num = settings.min;
				// field.value = num;
				applyValue(field, num, settings);
				$(field).triggerHandler("focus");
				$(field).triggerHandler("blur");
			}

			var id = e.target.parentElement.getAttribute("data-trigger-refresh");

			function Change() {
				setTimeout(function() {
					document.getElementById(id).click();
				}, 1500);
			}

			if(id) {
				if(document.getElementById(id)) {
					field.addEventListener("change", Change());
				}
				else {
					console.info("Кнопка '#"+ id +"', по якій емулюється клік, не знайдена");
				}
			}

			$('body').trigger('yamasters-counter:changed', {source: field});
		}
	});
};


exports.init = () => {
	var dataYamCounter = document.querySelectorAll("[data-yam-counter]");
	if(dataYamCounter.length) {
		yamCounter();
	}
}
