//////////////////////////////////////////////////////
// yamasters select

NodeList.prototype.yamSelect = function() {
	var dataYamSelect = this;

	dataYamSelect.forEach(function(el) {

		if (el.getAttribute("data-yam-select") == "created") {
			// console.log("was created, go to next");
		}

		else {
			// console.log('find not created, working...');

			var options = el.querySelectorAll('select option');

			if(options.length) {
				// console.log(options);

				var span = document.createElement("span");
				span.classList.add("value");

				var ul = document.createElement("ul");
				ul.classList.add("select");

				el.appendChild(span);
				el.appendChild(ul);

				var maximumOfItems = el.getAttribute("data-max-items");
				maximumOfItems ? true : maximumOfItems = 10;
				// console.log(maximumOfItems);

				var Core = {
					showList: function() {
						ul.classList.add("active");
					},
					hideList: function() {
						ul.classList.remove("active");
					}
				};

				el.querySelector("select").style.display = "none";

				var txt = false;

				options.forEach(function(option) {
					// console.log(option);
					if(option.selected) {
						txt = option.innerHTML;
					}
				});

				if(txt) {
					span.innerHTML = txt;
				}
				else {
					span.innerHTML = options[0].innerHTML;
				}

				for (var i = 0; i < options.length; i++) {
					var li = document.createElement("li");
					li.classList.add("option");
					li.id = options[i].id;

					for (var variable in options[i].dataset) {
						if (options[i].dataset.hasOwnProperty(variable)) {
							// console.log(options[i].dataset[variable]);
							li.dataset[variable] = options[i].dataset[variable];
						}
					}

					if(options[i].disabled) {
						li.classList.add("disabled");
					}

					li.setAttribute("data-value", options[i].value);
					li.innerHTML = options[i].innerHTML;

					ul.appendChild(li);
				}

				var H = 0;
				var li = ul.querySelectorAll("li");
				// console.log(ul, li);

				if(maximumOfItems && parseInt(maximumOfItems) < li.length) {
					for (var i = 0; i < maximumOfItems; i++) {
						H += li[i].clientHeight;
					}
					// console.log(maximumOfItems, H);

					ul.style.maxHeight = H + "px";
					ul.style.overflowY = "scroll";
					ul.style.overflowX = "hidden";
				}

				span.addEventListener("click", function() {
					var container = $(this).closest('[data-yam-select]')[0];

					if(container.classList.contains('active')) {
						container.classList.remove('active');
						Core.hideList();
					}
					else {
						dataYamSelect.forEach(function(s) {

							if(s.getAttribute('data-yam-select') == 'created') {

								s.querySelector(".select").classList.remove("active");
								s.classList.remove("active");
							}
						});

						container.classList.add("active");
						Core.showList();
					}
				});

				ul.querySelectorAll("li").forEach(function(li) {
					li.addEventListener("click", function() {
						var $this = this;

						var container = $(this).closest("[data-yam-select]")[0];
						container.querySelector(".value").innerHTML = this.innerHTML;

						container.querySelectorAll("option").forEach(function(option) {
							// console.log(option);
							if(option.selected) {
								option.selected = false;
							}
							if(option.value == $this.getAttribute("data-value")) {
								option.selected = true;
							}
						});

						$(container.querySelector("select")).trigger("change");

						container.classList.remove("active");

						container.querySelector(".select").classList.remove("active");
					});
				});

				document.addEventListener("click", function(e) {
					if(!$(e.target).closest("[data-yam-select]")[0]) {
						dataYamSelect.forEach(function(select) {
							if(select.getAttribute('data-yam-select') == 'created') {

								select.classList.remove("active");
								select.querySelector(".select").classList.remove("active");
							}
						});
					}
				})
			}

			el.setAttribute("data-yam-select", "created");
			el.classList.add("show");
		}
	});
}

exports.init = () => {
	var dataYamSelect = document.querySelectorAll("[data-yam-select]");
	if(dataYamSelect.length) {
		document.querySelectorAll("[data-yam-select]").yamSelect();
	}
}
