var buttons = document.querySelectorAll('.blockShare a');

let vw = window.innerWidth / 2;
let vh = window.innerHeight / 2;

let left = (window.innerWidth - vw) / 2;
let top = (window.innerHeight - vh) / 2;

let params = 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height='+ vh +',width='+ vw +',left='+ left +',top='+ top;

for(var i = 0; i < buttons.length; i++) {
		buttons[i].addEventListener('click', function(e) {
				e.preventDefault();

				let link = this.getAttribute('data-share-url');

				if(link) {
						if(this.classList.contains('twitter')) {
								window.open(
										'https://twitter.com/home?status='+ link,
										'', params
								);
						}
						else if(this.classList.contains('googleplus')) {
								window.open(
										'https://plus.google.com/share?url='+ link,
										'', params
								);
						}
						else if(this.classList.contains('facebook')) {
								window.open(
										'https://www.facebook.com/sharer/sharer.php?u='+ link,
										'', params
								);
						}
						else if(this.classList.contains('vkontakte')) {
								window.open(
										'http://vkontakte.ru/share.php?url='+ link,
										'', params
								);
						}
						else if(this.classList.contains('telegram')) {
								window.open(
										'https://telegram.me/share/url?url='+ link,
										'', params
								);
						}

						else if(this.classList.contains('linkedin')) {
								window.open(
										'https://www.linkedin.com/shareArticle?mini=true&url='+ link,
										'', params
								);
						}
						else if(this.classList.contains('pinterest')) {
								window.open(
										'https://pinterest.com/pin/create/button/?url='+ link,
										'', params
								);
						}
						else if(this.classList.contains('tumblr')) {
								window.open(
										'https://www.tumblr.com/widgets/share/tool?canonicalUrl='+ link,
										'', params
								);
						}
						else if(this.classList.contains('reddit')) {
								window.open(
										'https://reddit.com/submit?url='+ link,
										'', params
								);
						}
						else if(this.classList.contains('skype')) {
								window.open(
										'https://web.skype.com/share?url='+ link,
										'', params
								);
						}
						else if(this.classList.contains('viber')) {
								var a = document.createElement('a');
								a.href = 'viber://forward?text='+ link;
								a.click();
								a.remove();
						}
						else if(this.classList.contains('whatsapp')) {
								var a = document.createElement('a');
								a.href = 'whatsapp://send?text='+ link;
								a.click();
								a.remove();
						}
				}
				else {
						console.warn('Share button must have attribute "data-share-url"!');
				}
		});
}
